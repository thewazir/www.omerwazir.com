---
title: About Me
layout: page
tags: ['about','page']
pageOrder: 1
---

I'm Omer (sounds like boomer without the “B” <span class="line-through">b</span>oomer). I work as a software developer in Tucson, Arizona. I can speak three languages - English, Urdu and Pashto. I can read and write in English and Urdu. I've lived in the USA and Pakistan.

<!--
###Favorite Books
* [Professional JavaScript for Web Developers Third Edition](http://www.wrox.com/WileyCDA/WroxTitle/Professional-JavaScript-for-Web-Developers-3rd-Edition.productCd-1118026691.html) This was the book that taught me JavaScript. I still keep it at my desk as a reference.

###Favorite Applications

* [f.lux](http://justgetflux.com/) Adapt your computer display to the time of the day.
* [Sublime Text](http://www.sublimetext.com/2) I do most of my work now in Sublime Text. I'm not a power user when it comes to text editors. The most power I get is from having [Emmet](http://emmet.io/) installed and the ability to select and edit multiple things at once with `Ctrl-D`.
* [Chrome Canary](https://www.google.com/intl/en/chrome/browser/canary.html)
* [Firefox Aurora](http://www.mozilla.org/en-US/firefox/aurora/?WT.mc_id=fa10sn)
* [Typekit](https://typekit.com/)

####Linux
* [Docker](https://www.docker.io/) Docker has made it really easy for me to install and run applications on Linux.

####Windows

* [Cmder](http://bliker.github.io/cmder/) Great looking command prompt. It's hard to find a command prompt for windows that looks better than Cmder.
* [Fiddler](http://www.telerik.com/fiddler)
 Fantastic for inspecting traffic and extremley useful if you can take the time to learn how to use it. Also add the [Syntax-Highlighting Add-Ons](http://www.telerik.com/fiddler/add-ons) so you can edit rules for Fiddler.
 * [Intruder21](http://yamagata.int21h.jp/tool/intruder21/) I came across this extension while going through a security course and it proved to be very useful for illustrating how easily an insecure connection can be manipulated.
* [SharpKeys](http://sharpkeys.codeplex.com/) Easily remap keyboard keys
* [Github for Windows](https://windows.github.com/)
* [7Zip](http://www.7-zip.org/)

####OS X
* [Homebrew](http://brew.sh/)

###Favorite NPM packages
* [azure cli](https://www.npmjs.org/package/azure-cli)
* [docpad](https://www.npmjs.org/package/docpad)
* [gulp](https://www.npmjs.org/package/gulp)
* [grunt](https://www.npmjs.org/package/grunt)
* [nodeschool.io](http://http://nodeschool.io/)
* [http-server](https://www.npmjs.org/package/http-server) Don't need Python's SimpleHTTPServer anymore

###Chrome extensions
* [Ghostery](https://www.ghostery.com/en/)

###Video Games
* Grim Fandango
* KOTOR
* Mass Effect Series

###The Broncos.
-->
<!-- 
MDN profile https://developer.mozilla.org/en-US/profiles/OWaz
Github https://github.com/thewazir
StackOverflow https://stackoverflow.com/users/1650294/owaz
Twitter https://twitter.com/thewazir
-->