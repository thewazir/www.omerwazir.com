---
title: Using a Docker image to run Jenkins
layout: post
tags: ['docker','jenkins','continuous-integration','azure']
lead: "When I needed to run Jenkins for a personal project I decided to use a Docker image. Surprisingly it was very simple to do and I'll explain what it was that I did"
date: 2014-03-31 22:59:23
---
### Why Docker?

### Installing Docker

### Finding a Jenkins image

### Tips
#### Running an image and starting a container
In the very early stages of using docker I incorrectly would try to run a previously stopped/killed image thinking that my data would persist. The image would load and I would be super sad to realize my data was gone. I was completely ignoring the difference between images and containers. 


### Final words