---
title: Safari won't create a WSS connection if the certificate isn't signed by a known CA
layout: post
tags: ['safari','security','websockets']
lead: "While building an app that used web sockets I learned that Safari won't create a secure web socket connection if the certificate being used isn't signed by a known CA"
date: 2014-04-05 23:01:33
---
### What I was doing
### How I noticed the issue
### How I debugged the issue
### How I fixed the issue
