---
title: Using a Docker image to run Jenkins
layout: post
tags: ['docker','jenkins','continuous-integration','azure']
lead: "When I needed to run Jenkins for a personal project I decided to use a Docker image. Surprisingly it was very simple to do and I'll explain what it was that I did"
date: 2014-03-31 22:59:23
---

```
<div class="cA-cardsSingleCol">

<!--<a class="cA-cardsGoBack" href="chip.html">&lt; Back to Chip Technology Page</a>
-->
<a class="cA-cardsGoBackChip" href="javascript:window.history.go(-1);"><span></span> Back to Chip Technology Page</a>

<!-- end of main column -->
</div>
```