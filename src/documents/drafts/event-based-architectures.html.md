---
title: Notes on Event Based Architectures
layout: post
tags: ['javascript','book notes']
lead: "I've written a collection of notes while reading Chapter 3 of Testable Javascript. Please don't assume what I've written is completely correct, these notes reflect my interpretation of the book's contents."
date: 2014-05-13 15:43:24
---

### Using events to reduce complexity